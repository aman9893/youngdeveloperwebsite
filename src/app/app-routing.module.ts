import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './protected-component/dashboard/dashboard.component';
import { SlideBarComponent } from './protected-component/slide-bar/slide-bar.component';
import { ModuleWithProviders } from '@angular/core';
import { ContactComponent } from './protected-component/contact/contact.component';

export const  AppRoutes: Routes = [
     { path: ' ', component: DashboardComponent },
     { path: 'dashboard', component: DashboardComponent },
     { path: 'slidebar', component: SlideBarComponent },
     { path: 'contact', component: ContactComponent },

     
     
];

export const ROUTING: ModuleWithProviders = RouterModule.forRoot(AppRoutes);
