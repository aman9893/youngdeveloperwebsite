import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { MaterialModule } from './other-module/material'
import { ROUTING } from './app-routing.module';
import { HeaderComponent } from './protected-component/header/header.component';
import { FooterComponent } from './protected-component/footer/footer.component';
import { SlideBarComponent } from './protected-component/slide-bar/slide-bar.component';
import { DashboardComponent } from './protected-component/dashboard/dashboard.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './protected-component/home/home.component';
import { ContactComponent } from './protected-component/contact/contact.component';
import { AboutComponent } from './protected-component/about/about.component';
import { ProjectComponent } from './protected-component/project/project.component';
import { PortfolieComponent } from './protected-component/portfolie/portfolie.component';
import { AddContactComponent } from './protected-component/contact/add-contact/add-contact.component';
// import { NganimationComponent } from './nganimation/nganimation.component';
// import { AnimateOnScrollModule } from 'ng2-animate-on-scroll';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddContactService } from './service/add-contact.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    SlideBarComponent,
    DashboardComponent,
    HomeComponent,
    ContactComponent,
    AboutComponent,
    ProjectComponent,
    PortfolieComponent,
    AddContactComponent
    // NganimationComponent,
    
  ],
  imports: [
    BrowserModule,
    ROUTING,
    MaterialModule,BrowserAnimationsModule,
    FormsModule,ReactiveFormsModule
    // AnimateOnScrollModule.forRoot()
  ],
  providers: [AddContactService],
  bootstrap: [AppComponent],
  entryComponents:[AboutComponent,AddContactComponent]
})
export class AppModule { }
