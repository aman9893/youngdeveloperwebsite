import { Component, OnInit } from '@angular/core';
declare var particlesJS: any;
import * as  particlesJS from 'particles.js';
@Component({
  selector: 'app-slide-bar',
  templateUrl: './slide-bar.component.html',
  styleUrls: ['./slide-bar.component.css']
})
export class SlideBarComponent implements OnInit {

  
  constructor() { }

  ngOnInit() {
    particlesJS.load('particles-js', 'assets/particles.json', function() {
        console.log('callback - particles.js config loaded');
      });
  }

}
