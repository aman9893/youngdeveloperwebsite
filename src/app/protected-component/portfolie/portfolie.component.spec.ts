import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PortfolieComponent } from './portfolie.component';

describe('PortfolieComponent', () => {
  let component: PortfolieComponent;
  let fixture: ComponentFixture<PortfolieComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PortfolieComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PortfolieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
