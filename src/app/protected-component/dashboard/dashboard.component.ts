import { Component,HostListener,ElementRef,OnInit} from '@angular/core';
import { trigger,state, style, animate, transition} from '@angular/animations';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  animations: [
    trigger('scrollAnimation', [
      state('show', style({
        opacity: 1,
        transform: "translateX(0)"
      })),
      state('hide',   style({
        opacity: 0,
        transform: "translateX(-20%)"
      })),
      transition('show => hide', animate('700ms ease-out')),
      transition('hide => show', animate('800ms ease-in'))
    ])
  ]
})
    

export class DashboardComponent implements OnInit {
      state = 'hide'
  // currentState = 'initial';
  // @Input() currentState;

  constructor(public el: ElementRef) { }
  @HostListener('window:scroll', ['$event'])
  checkScroll() {
    const componentPosition = this.el.nativeElement.offsetTop
    const scrollPosition = window.pageYOffset

    if (scrollPosition >= componentPosition) {
      this.state = 'show'
    } 
    // else {
    //   this.state = 'hide'
    // }

  }
   ngOnInit() {
   }

  // changeState(){   
  //   this.currentState = this.currentState === 'initial' ? 'final' : 'initial';
  // }
  }


