import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  showNotiDiv= false;
  constructor() { }

  ngOnInit() {
  }
  shownotidiv(){
  this.showNotiDiv=!this.showNotiDiv;
  }
}
