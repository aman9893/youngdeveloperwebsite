import { Component, OnInit } from '@angular/core';
import { AddContactComponent } from './add-contact/add-contact.component';
import { MatDialog , MatDialogConfig } from '@angular/material';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {


  constructor(public dialog: MatDialog) { }

  ngOnInit() {
    
  }
  openDialog()
  {
    const dialogConfig = new MatDialogConfig(); 
    dialogConfig.disableClose=true;
    dialogConfig.autoFocus=true;
    dialogConfig.width="45%";
    this.dialog.open(AddContactComponent,dialogConfig);
  }

}
