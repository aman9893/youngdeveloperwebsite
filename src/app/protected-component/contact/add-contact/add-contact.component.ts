import { Component, OnInit } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';



@Component({
  selector: 'app-add-contact',
  templateUrl: './add-contact.component.html',
  styleUrls: ['./add-contact.component.css']
})
export class AddContactComponent implements OnInit {
  ContactForm: FormGroup;
  submitted = false;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
      this.ContactForm = this.formBuilder.group({
          firstName: ['', Validators.required],
          lastName: ['', Validators.required],
          email: ['', [Validators.required, Validators.email]],
          phonenumber: ['', [Validators.required, Validators.minLength(10)]],
         
      });
  }

  // convenience getter for easy access to form fields
  get f() { return this.ContactForm.controls; }


  

  onSubmit() {

    console.log("Aman hi");
     console.log(this.ContactForm);

  }




 
}